#Import libraries

from dotenv import load_dotenv
import os
import datetime
import logging
from pathlib import Path
import jinja2
import utils
#Add function upload files to S3

FOLDERPATH = 'media/'

# The `UploadFilesS3` class provides methods for validating and uploading files to an S3 bucket, and
# processing the uploaded files to extract and compare addresses.
class UploadFilesS3():
    """Initialize the UploadFilesS3 class.
        Args:
            configs (dict): A dictionary containing configuration parameters.
        Returns:
            None
        """
    def __init__(self, configs):
        self._configs = configs
        self.address_mapping = []

    def load_files_S3(self, folderpath, path_adrresshomogens):
        self.validate_folder(folderpath)
        filepaths = self.get_filepaths(folderpath)
        self.validate_files(filepaths)
        self.upload_files(filepaths, path_adrresshomogens)

    def validate_folder(self, folderpath):
        if not os.path.isdir(folderpath):
            raise FileNotFoundError(f"Directory '{folderpath}' does not exist.")

    def get_filepaths(self, folderpath):
        return [str(path) for path in Path(folderpath).iterdir() if path.is_file()]

    def validate_files(self, filepaths):
        if not filepaths:
            raise Exception(f"No files found in directory '{folderpath}'.")

    def upload_files(self, filepaths, path_adrresshomogens):
        for path in filepaths:
            try:
                with open(path, 'r') as f:
                    logging.debug(path)
                    print(path)
                    extension = f'{path.split(".")[1]}'
                    filename = f'{path.split(".")[0].replace(" ", "").split("/")[1]}'
                    ext_validate = utils.validate_type_files(extension)
                    if ext_validate:
                        new_filename = f'{path.split(".")[0]}_{datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")}.pdf'
                        configs.Bucket(os.environ.get('ENV_AWS_BUCKET_NAME')).upload_file(path, new_filename)
                        print("Done Upload File")
                        if 'pdf' in extension:
                            content = utils.read_text_pdf(path)
                            self.process_content_files(content, filename, path_adrresshomogens)
                            
                        elif 'json' in extension or 'csv' in extension or 'txt' in extension or 'doc' in extension:
                            content = f.read()
                            self.process_content_files(content, filename, path_adrresshomogens)
                    else:
                        raise Exception(f"File not allowed for processing  {path.split('/')[1]} ")
            except Exception as e:
                logging.error(f'Error uploading file {path} to S3 bucket {os.environ.get("ENV_AWS_BUCKET_NAME")}: {e}')

    def process_content_files(self, content, filename, path_adrresshomogens):
        address_homogens_all = utils.process_text(text=content, pos=3, path_address="./media/out/address")
        if address_homogens_all[0] != "Not address" and address_homogens_all[0] is not None and address_homogens_all[0] is not False:
            address = utils.extract_address(content)
            similarity_threshold = int(os.environ.get('SIMILARITY_THRESHOLD'))
            call_comparision_address = utils.compare_address(address_original=address, homonymous_addresses=address_homogens_all, similarity_threshold=similarity_threshold)    
            
            if call_comparision_address is not False:
                file_mapping = (filename, call_comparision_address)  # Crear una tupla con el nombre del archivo y las direcciones homogéneas
                self.address_mapping.append(file_mapping)  
                path_adrresshomogens += f'{filename}.csv'
                utils.save_similar_addresses(call_comparision_address, path_adrresshomogens)
            else:
                pass
        return self.address_mapping    


if __name__ == "__main__":
    load_dotenv()
    # Load configurations
    configs = utils.configurations()
    # Create instance UploadFilesS3
    uploader = UploadFilesS3(configs)
    # Call method load_files_S3 with route of the folder
    folderpath = input("Hello, enter the path where your files are: ")
    path_adrresshomogens = "./media/out/mapping_address/"

    uploader.load_files_S3(folderpath, path_adrresshomogens)

   # Obtener las listas separadas de nombres de archivo y direcciones homogéneas
    file_names = [file_mapping[0] for file_mapping in uploader.address_mapping]
    homogeneous_addresses = [file_mapping[1] for file_mapping in uploader.address_mapping]

    # Imprimir los nombres de archivo y las direcciones homogéneas
    #print(file_names)
    #print(homogeneous_addresses)

    # Obtener las coordenadas de las direcciones homogéneas
    address_coordinates = []
    for addresses in homogeneous_addresses:
        coords = []
        for address in addresses:
            coordinate = utils.get_coordinates_from_address(address)
            if coordinate is not None:
                coords.append(coordinate)
        address_coordinates.append(list(zip(addresses, coords)))
    from pprint import pprint

    # Crear un archivo HTML para cada tupla de direcciones homogéneas
    for i, data in enumerate(address_coordinates):
        utils.generate_html_file(file_names[i], data)
        salida = [[coord[1][0], coord[1][1], coord[0]] for coord in data]

        list_coords = [[c[0], c[1]] for c in salida]
        list_addresess = [d[2] for d in salida]
        object_map = utils.agregar_marcadores(list_addresess, list_coords)
        name_file_map = f"{file_names[i]}_mapa.html"
        object_map.save(name_file_map)
        print("---"*15)
    

else:
    print("File one executed when imported")
