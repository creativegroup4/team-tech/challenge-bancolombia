import unittest
from main import *
import os 
import utils
import boto3
import botocore

MEDIA_FOLDER_PATH = 'media/'

class TestS3Upload(unittest.TestCase):
    
    # Tests that all files in the specified folder are uploaded to S3 bucket
    def test_upload_files(self):
        configs = boto3.resource('s3')
        folderpath = r"media/"
        filepaths  = [os.path.join(folderpath, name) for name in os.listdir(folderpath)]
        all_files = []
        for path in filepaths:
            with open(path, 'rb') as f:
                file = f.read()
                all_files.append(file)
                configs.Bucket(os.environ.get('ENV_AWS_BUCKET_NAME')).put_object(Key = str(datetime.datetime.now()) + '-metodonuevo.pdf', Body =file)
        assert len(all_files) == len(filepaths)

    # Tests that all files are uploaded with a unique key based on current datetime
    def test_upload_files_with_unique_key(self):
        configs = boto3.resource('s3')
        folderpath = r"media/"
        filepaths  = [os.path.join(folderpath, name) for name in os.listdir(folderpath)]
        all_files = []
        for path in filepaths:
            with open(path, 'rb') as f:
                file = f.read()
                all_files.append(file)
                configs.Bucket(os.environ.get('ENV_AWS_BUCKET_NAME')).put_object(Key = str(datetime.datetime.now()) + '-metodonuevo.pdf', Body =file)
        keys = [obj.key for obj in configs.Bucket(os.environ.get('ENV_AWS_BUCKET_NAME')).objects.all()]
        assert len(keys) == len(filepaths)

    # Tests that an error is logged when folder path does not exist
    def test_folder_path_does_not_exist(self):
        configs = boto3.resource('s3')
        folderpath = r"nonexistent/"
        filepaths  = [os.path.join(folderpath, name) for name in os.listdir(folderpath)]
        all_files = []
        for path in filepaths:
            try:
                with open(path, 'rb') as f:
                    file = f.read()
                    all_files.append(file)
                    configs.Bucket(os.environ.get('ENV_AWS_BUCKET_NAME')).put_object(Key = str(datetime.datetime.now()) + '-metodonuevo.pdf', Body =file)
            except FileNotFoundError:
                assert str(e) == "[Errno 2] No such file or directory: 'nonexistent/'"
            except UnicodeDecodeError:
                assert str(e) == "'utf-8' codec can't decode byte 0xff in position 0: invalid start byte"
            except botocore.exceptions.ClientError:
                assert str(e) == "The AWS Access Key Id you provided does not exist in our records."


    # Tests that an error is logged when file path does not exist
    def test_file_path_does_not_exist(self):
        configs = boto3.resource('s3')
        folderpath = r"media/"
        filepaths  = [os.path.join(folderpath, "nonexistent.pdf")]
        all_files = []
        for path in filepaths:
            try:
                with open(path, 'rb') as f:
                    file = f.read()
                    all_files.append(file)
                    configs.Bucket(os.environ.get('ENV_AWS_BUCKET_NAME')).put_object(Key = str(datetime.datetime.now()) + '-metodonuevo.pdf', Body =file)
            except Exception as e:
                assert str(e) == "[Errno 2] No such file or directory: 'media/nonexistent.pdf'"

    # Tests that an error is logged when file cannot be read
    def test_file_cannot_be_read(self):
        configs = boto3.resource('s3')
        folderpath = r"media/"
        filepaths  = [os.path.join(folderpath, name) for name in os.listdir(folderpath)]
        all_files = []
        for path in filepaths:
            try:
                with open(path, 'w') as f:
                    f.write("test")
                with open(path, 'rb') as f:
                    file = f.read()
                    all_files.append(file)
                    configs.Bucket(os.environ.get('ENV_AWS_BUCKET_NAME')).put_object(Key = str(datetime.datetime.now()) + '-metodonuevo.pdf', Body =file)
            except Exception as e:
                assert str(e) == "'utf-8' codec can't decode byte 0xff in position 0: invalid start byte"

    # Tests that an error is logged when AWS credentials are invalid
    def test_invalid_aws_credentials(self):
        configs = boto3.resource('s3', aws_access_key_id='invalid', aws_secret_access_key='invalid')
        folderpath = r"media/"
        filepaths  = [os.path.join(folderpath, name) for name in os.listdir(folderpath)]
        all_files = []
        for path in filepaths:
            try:
                with open(path, 'rb') as f:
                    file = f.read()
                    all_files.append(file)
                    configs.Bucket(os.environ.get('ENV_AWS_BUCKET_NAME')).put_object(Key = str(datetime.datetime.now()) + '-metodonuevo.pdf', Body =file)
            except Exception as e:
                assert str(e) == "The AWS Access Key Id you provided does not exist in our records."


    # Tests that the function generates homonymous addresses for a valid address
    def test_valid_address(self):
        original_address = 'Calle 123'
        expected_output = ['Kalle 123', 'Carrera 123', 'Carrera Nro 123', 'Carrera Numero 123', 'Carrera Num 123', 'Trasversal 123']
        assert utils.generate_homonymous_addresses(original_address) == expected_output

    # Tests that the function generates homonymous addresses for different variations of the same address
    def test_same_address_variations(self):
        original_address = 'CRA 123 # 456'
        expected_output = ['Kra 123 # 456', 'Carrera 123 # 456', 'Carrera Nro 123 # 456', 'Carrera Numero 123 # 456', 'Carrera Num 123 # 456', 'Calle 123 # 456', 'Trasversal 123 # 456']
        assert utils.generate_homonymous_addresses(original_address) == expected_output

    # Tests that the function generates homonymous addresses for an address containing 'Numero', 'Nro' or 'Num'
    def test_address_with_numero_nro_num(self):
        original_address = 'CRA 123 Nro # 456'
        expected_output = ['Kra 123 # 456', 'Carrera 123 # 456', 'Carrera Nro 123 # 456', 'Carrera Numero 123 # 456', 'Carrera Num 123 # 456', 'Calle 123 # 456', 'Trasversal 123 # 456']
        assert utils.generate_homonymous_addresses(original_address) == expected_output

    # Tests that the function returns False for an empty address
    def test_empty_address(self):
        original_address = ''
        expected_output = False
        assert utils.generate_homonymous_addresses(original_address) == expected_output

    # Tests that the function returns False for an address that does not contain any of the patterns
    def test_address_without_patterns(self):
        original_address = 'This is not an address'
        expected_output = False
        assert utils.generate_homonymous_addresses(original_address) == expected_output

    # Tests that the function generates homonymous addresses for an address containing special characters
    def test_address_with_special_characters(self):
        original_address = 'CRA 123 # 456, Apto. 789'
        expected_output = ['Kra 123 # 456, Apto. 789', 'Carrera 123 # 456, Apto. 789', 'Carrera Nro 123 # 456, Apto. 789', 'Carrera Numero 123 # 456, Apto. 789', 'Carrera Num 123 # 456, Apto. 789', 'Calle 123 # 456, Apto. 789', 'Trasversal 123 # 456, Apto. 789']
        assert utils.generate_homonymous_addresses(original_address) == expected_output

