
import boto3
import unicodedata
import pdftotext
import re
import csv
import os
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import googlemaps
import jinja2
import folium
import itertools
import folium
from folium import plugins


def configurations():
    """
     Function that creates the initial configurations to connect to AWS
     @return: (file) object for aws manipulation
     """
    s3_client = boto3.resource(
                    's3',
                    aws_access_key_id = os.environ.get('ENV_AWS_ACCESS_KEY_ID'),
                    aws_secret_access_key = os.environ.get('ENV_AWS_SECRET_ACCESS_KEY'),
                    region_name=   os.environ.get('ENV_REGION_NAME')
            )
    return s3_client

def read_pdf(ruta_pdf: str ):
    """
     Function that reads a pdf file
     @param path_pdf: location of the pdf file to read
     @return: (file) PDF file available for processing
     """
    #print('----------------- Read_pdf ---------------------')
    file = open(ruta_pdf, "rb")
    file_read_pdf = pdftotext.PDF(file)
    return file_read_pdf

def read_file(route: str ):
    """
     Function that reads a file
     @param path_pdf: location of the pdf file to read
     @return: (file) PDF file available for processing
     """
    file = open(route, "rb")
    #print(archivo)
    readfile = pdftotext.PDF(file)
    return readfile

def read_text_pdf(route_pdf: str ):
    """
     Function that reads a pdf file and returns its content in text format
     @param path_pdf: location of the pdf file to read
     @return: (str) Text of the pdf
     """
    extractfile_pdf = read_pdf(route_pdf)
    text = ""
    for page in extractfile_pdf:
        page = "{} ".format(page)
        text += unicodedata.normalize("NFKC", page)
    return text

def validate_type_files(ext):
    """
     Function that validates what type of extension is an allowed file
     @param ext: type of extension
     @return: (ext) True or False validation
     """
    valid_extensions = ['json','pdf','txt','doc','csv']
    if not ext.lower() in valid_extensions:
        return False
    else:
        return True

def extract_address(original_text):
    """
     Function that looks for a possible address in the received text
     @param original_text: text to extract the address
     @return: (address) Returns the address found or, failing that, None
     """
    match = re.search(r'(CRA|Kra|Carrera|Calle|Trasversal)\s*\d+\s*#*\s*\d+[A-Za-z]*\s*(-\s*\d+)?', original_text)
    if match:
        address = match.group()
        return address
    return None

def generate_homonymous_addresses(original_address):
    """
     Function that creates homogeneous addresses from an initial address
     @param original_address: address
     @return: (homonymous_addresses) Returns the homogeneous addresses created from the received address
     """
    if not original_address:
        return False
    homonymous_addresses = []
    # Regular expressions for different variations of addresses
    patterns = [
        (r'(CRA)\s*(\d+)\s*#*\s*(\d+)[A-Za-z]*\s*(-\s*\d+)?', ['Kra', 'Carrera', 'Carrera Nro', 'Carrera Numero', 'Carrera Num', 'Calle', 'Trasversal']),
        (r'(Kra)\s*(\d+)\s*#*\s*(\d+)[A-Za-z]*\s*(-\s*\d+)?', ['CRA', 'Carrera', 'Carrera Nro', 'Carrera Numero', 'Carrera Num', 'Calle', 'Trasversal']),
        (r'(Carrera)\s*(\d+)\s*#*\s*(\d+)[A-Za-z]*\s*(-\s*\d+)?', ['CRA', 'Kra', 'Carrera Nro', 'Carrera Numero', 'Carrera Num', 'Calle', 'Trasversal']),
        (r'(Calle)\s*(\d+)\s*#*\s*(\d+)[A-Za-z]*\s*(-\s*\d+)?', ['CRA', 'Kra', 'Carrera', 'Carrera Numero', 'Carrera Num', 'Carrera Nro', 'Trasversal']),
        (r'(Trasversal)\s*(\d+)\s*#*\s*(\d+)[A-Za-z]*\s*(-\s*\d+)?', ['CRA', 'Kra', 'Carrera', 'Carrera Numero', 'Carrera Num', 'Carrera Nro', 'Calle'])
    ]
    # Find all homonymous addresses in the original address
    for pattern, replacements in patterns:
        matches = re.findall(pattern, original_address)
        for match in matches:
            for replacement in replacements:
                homonymous_address = original_address.replace(match[0], replacement, 1)
                if any(word in homonymous_address for word in ['Numero', 'Nro', 'Num']):
                    homonymous_address = homonymous_address.replace("#", "")
                    words = homonymous_address.split()
                    first_word = words[0]
                    other_words = words[1:]
                    three_word = other_words.pop(1)
                    other_words.insert(0, three_word)
                    homonymous_address = " ".join([first_word] + other_words)
                homonymous_addresses.append(homonymous_address)
    if len(homonymous_addresses) == 0:
        return False
    return homonymous_addresses

def process_text(text, pos, path_address):
    output = []
    # Extract address
    original_address = extract_address(text)
    if original_address:
        homonymous_addresses = generate_homonymous_addresses(original_address)
        if homonymous_addresses:
            # Check if the folder already exists    
            if not os.path.exists(path_address):
                os.makedirs(path_address, exist_ok=True)
            else:
                print(f"Folder already exists: {path_address}")
            path_end = f"{path_address}/address_homofons_{pos}.csv"
            with open(path_end, 'w', newline='') as file:
                writer = csv.writer(file)
                #writer.writerow(['Direcciones:'])
                for address in homonymous_addresses:
                    writer.writerow([address])
            return homonymous_addresses
        else:
            return homonymous_addresses
    else:
        output = ['Not address']
        return output

def save_similar_addresses(similar_addresses, file_path):
    with open(file_path, 'w') as file:
        for address in similar_addresses:
            file.write(address + '\n')

def compare_address(address_original, homonymous_addresses, similarity_threshold = 80  ):
    # Lista para almacenar las direcciones con similitud superior al 80%
    similar_addresses = []
    for address in homonymous_addresses:
        similarity_ratio = fuzz.ratio(address_original, address)
        if similarity_ratio >= similarity_threshold:
            similar_addresses.append(address)
    # Imprimir las direcciones similares encontradas
    if len(similar_addresses) == 0:
        return False
    else:
        return similar_addresses
        for address in similar_addresses:
            pass
            #print(f"Dirección similar: {address}")
    
def get_coordinates_from_address(address):

    return '3.445066294015349', '-76.51055146974366'
    gmaps = googlemaps.Client(key=os.environ.get('ENV_REGION_NAME'))
    geocode_result = gmaps.geocode(address)
    # Check if geocoding results were returned
    if len(geocode_result) > 0:
        # Extract the coordinates of the geocoded location
        location = geocode_result[0]['geometry']['location']
        latitude = location['lat']
        longitude = location['lng']
        return latitude, longitude
    return None

def zip_lists(list1, list2):
    return list(itertools.zip_longest(list1, list2))

def generate_html_file(file_name, address_coordinates):
    env = jinja2.Environment(loader=jinja2.FileSystemLoader('.'))
    template = env.get_template('template.html')
    rendered_content = template.render(filename=file_name, address_coordinates=address_coordinates)
    with open(f"{file_name}.html", 'w') as file:
        file.write(rendered_content)

def agregar_marcadores(addresses, coords):
    mapa = folium.Map(location=coords[0], zoom_start=12)
    for address, coordenada in zip(addresses, coords):
        folium.Marker(location=coordenada, popup=address).add_to(mapa)
    return mapa