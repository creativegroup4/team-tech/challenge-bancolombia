# Automatización de Carga y Análisis de Documentos


Esta solución automatiza la carga y análisis de documentos para aliviar la carga operativa de la empresa. Permite subir uno o varios documentos a un sistema de almacenamiento en la nube, extraer la dirección del documento en texto plano, crear una base de datos de direcciones homónimas, comparar las direcciones originales con las homónimas, almacenar las direcciones similares en una nueva fuente de datos y obtener las coordenadas exactas de las direcciones utilizando el API de Google Maps para presentarlas en un mapa.

# Descripción del Problema

La empresa procesa cientos de miles de documentos de los clientes que se requieren registrar en nuestra plataforma. Para aliviar nuestra carga operativa, necesitamos una solución que nos permita automatizar la carga y análisis de dichos documentos. El objetivo es subir los documentos a un sistema de almacenamiento en la nube, extraer la dirección del documento en texto plano, generar una lista de direcciones homónimas y realizar comparaciones para almacenar las direcciones con alta similitud en una nueva fuente de datos. Finalmente, se utilizará el API de Google para obtener las coordenadas exactas de las direcciones y presentarlas en un mapa.

# Requisitos

El proyecto cumple con los siguientes requisitos:

* El código está escrito en Python.
* El código sigue estándares de programación, siendo limpio y legible.
* Se incluyen pruebas unitarias para verificar la funcionalidad del código.
* El código está versionado en un repositorio de control de versiones, como GitHub, GitLab o Bitbucket.
*Se proporciona un archivo README con instrucciones paso a paso para ejecutar el proyecto, con la mínima configuración necesaria.
* El sistema se ejecuta en la consola, sin requerir una interfaz visual.

#Configuración

Sigue los siguientes pasos para configurar y ejecutar el proyecto:

1. Clona el repositorio desde GitHub:

```
git clone git@gitlab.com:creativegroup4/team-tech/challenge-bancolombia.git

git clone https://gitlab.com/creativegroup4/team-tech/challenge-bancolombia.git
```


2. Accede al directorio del proyecto:

```
cd nombre_del_proyecto
```

3. Instala las dependencias utilizando pip:

```
pip install -r requirements.txt
```

4. Crea un archivo .env en la raíz del proyecto con la siguiente configuración:

```
# Replace the following placeholders with your own values
ENV_TYPE_BUCKET='s3'
ENV_AWS_ACCESS_KEY_ID='TU_ACCESS_KEY_ID'
ENV_AWS_SECRET_ACCESS_KEY='TU_SECRET_ACCESS_KEY'
ENV_AWS_BUCKET_NAME='NOMBRE_DEL_BUCKET'
ENV_LOCAL_FOLDER_PATH='/media/'
ENV_REGION_NAME='us-east-1'
GOOGLE_MAP_API_KEY='TU_API_KEY'
SIMILARITY_THRESHOLD=80
```
Asegúrate de reemplazar los valores de ENV_AWS_ACCESS_KEY_ID, ENV_AWS_SECRET_ACCESS_KEY, ENV_AWS_BUCKET_NAME y GOOGLE_MAP_API_KEY con tus propias credenciales y configuraciones.


5.  Aws CLI


(https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)

```
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```

6. Despues de instalar aws cli, debes configurar el entorno

```
aws configure

a. Proporcionar el AWS Access Key ID
b. AWS Secret Access Key
c. Default region name
d. Default output format [enter]
```


## Instrucciones de Uso

1. Ejecuta el archivo main.py desde la línea de comandos:
```
python main.py
```

2. Se te solicitará que ingreses la ruta donde se encuentran tus archivos. Ingresa la ruta y presiona Enter.

3. El programa subirá los archivos al sistema de almacenamiento en la nube configurado (por ejemplo, AWS S3) y realizará la extracción y análisis de las direcciones.

4. El programa generará un archivo de salida con las direcciones homónimas encontradas en la ruta ./media/out/mapping_address/.

5. El programa utilizará el API de Google Maps para obtener las coordenadas exactas de las direcciones y generará un mapa HTML para cada conjunto de direcciones homólogas encontrado en la ruta ./media/out/mapping_address/.

6. Para ver los resultados, abre los archivos HTML generados en un navegador web.

¡Listo! Ahora puedes ejecutar el proyecto y automatizar la carga y análisis de tus documentos.

Licencia
Este proyecto se distribuye bajo la licencia MIT. Consulta el archivo LICENSE para más detalles.


## Appendix

⌨️ Por Jhon Insuasti 😊