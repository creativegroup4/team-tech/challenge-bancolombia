import folium

def agregar_marcadores(addresses, coordenadas):
    # Crear un objeto de mapa centrado en las primeras coordenadas
    mapa = folium.Map(location=coordenadas[0], zoom_start=12)
    
    for address, coordenada in zip(addresses, coordenadas):
        folium.Marker(location=coordenada, popup=address).add_to(mapa)
    
    return mapa

# Lista de coordenadas de los marcadores
lista_coordenadas = [['3.445066294015349', '-76.51055146974366'], ['3.447679407032978', '-76.5117638261593']]
# Lista de direcciones para los marcadores
lista_direcciones = ['Kra 70 # 26A - 33', 'Calle 70 # 26A - 33']



# Llamar a la función para crear el mapa y agregar los marcadores
mapa = agregar_marcadores(lista_direcciones, lista_coordenadas)

# Guardar el mapa en un archivo HTML
mapa.save('mapa.html')
